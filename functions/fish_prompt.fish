function fish_prompt
        source $HOME/.config/fish/conf.d/dracula-fishline.fish
        fishline -s $status SIGSTATUS USERHOST FULLPWD GIT CLOCK N VIMODE
        echo " "
end
